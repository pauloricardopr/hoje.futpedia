function FutpediaHoje() {
    var _this = this;
    $.getJSON("http://futpedia.globo.com/widget/jogoshistoricos/hoje.json", function(data) {
        _this.showJogos(data);
    });
    this.MESES = ['Janeiro', 'Fevereiro', 'Março', 
                  'Abril', 'Maio', 'Junho', 
                  'Julho', 'Agosto', 'Setembro', 
                  'Outubro', 'Novembro', 'Dezembro'];
}

FutpediaHoje.prototype.showJogos = function(jogos) {
    var div;

    for (var i = 0, jogo, limite = jogos.length; i < limite; i++) {
        jogo = jogos[i];
        div = document.createElement("div");
        div.innerHTML = this.montarJogo(jogo);
        var carrossel = document.getElementById("ge-chrext-carrossel");
        carrossel.appendChild(div);
    }

    $(".ge-chrext-tooltip").geTooltip();
};

FutpediaHoje.prototype.div = function(content, opts) {
    var abre = "<div";
    var fecha = "</div>";
    
    if (opts && opts.clazz) {
        abre += ' class="'+opts.clazz+'"';
    }
    if (opts && opts.id) {
        abre += ' id="'+opts.id+'"';
    }
    abre +=">";

    return abre + content + fecha;
};

FutpediaHoje.prototype.montarJogo = function(jogo) {
    return this.montarInfo(jogo) + this.div(this.linha(154) + this.linha(62)) + this.montarPlacar(jogo);
};

FutpediaHoje.prototype.montarInfo = function(jogo) {
    return this.div(this.montarDataLegivel(jogo.data) + this.montarCampeonato(jogo), {clazz: "ge-chrext-info-jogo"});
};

FutpediaHoje.prototype.montarPlacar = function(jogo) {
    var mandante = jogo.equipe_mandante;
    var visitante = jogo.equipe_visitante;
    var placar = this.div(this.montarEscudo(mandante), {clazz: "ge-chrext-mandante placar"}) 
                 + this.div(jogo.placar_mandante, {clazz: "ge-chrext-placar-mand placar"}) 
                 + this.div(" <img src='/img/versus.gif' width='15' height='15'> ", {clazz: "ge-chrext-versus placar"}) 
                 + this.div(jogo.placar_visitante, {clazz: "ge-chrext-placar-vis placar"}) 
                 + this.div(this.montarEscudo(visitante), {clazz: "ge-chrext-visitante placar"});
    return this.div(placar, {clazz: "ge-chrext-placar"});
};

FutpediaHoje.prototype.montarEscudo = function(time) {
  var image = '<img src="'+time.escudo_medio+'" width="45px" height="45px">';
  return this.div(this.div(image + this.montarTooltip(time.nome_popular, "center"), {clazz:"ge-chrext-escudo"}));
};

FutpediaHoje.prototype.montarDataLegivel = function(data) {
    data = data.split("-");
    var dataLegivel = 'em ' + data[2] + ' de ' + this.MESES[parseInt(data[1], 10) - 1].toLowerCase() + ' de ' + data[0];
    return this.div(dataLegivel, {clazz: "ge-chrext-data"});
};

FutpediaHoje.prototype.montarCampeonato = function(jogo) {
    return this.div(jogo.campeonato.nome.toLowerCase(), {clazz: "ge-chrext-campeonato"});
};

FutpediaHoje.prototype.montarTooltip = function(texto, align) {
  return '<div class="ge-chrext-tooltip '+align+'">'
    + '<div class="content">'+texto+'</div>'
    + '<div class="background"></div>'
    + '<div class="arrow '+align+'"><img src="/img/tooltip-arrow.png"></div>'
    + '</div>';
};

FutpediaHoje.prototype.linha = function(size) {
  var midSize = size - 8;
  return '<TABLE class="line" BORDER="0" CELLPADDING="0" CELLSPACING="0" height="1px" width="'+size+'px" align="center">\
            <TR>\
              <TD class="end" bgcolor="#cecece"><img src="/img/pixel.gif" class="pix"></TD>\
              <TD class="trans" bgcolor="#d7d7d7"><img src="/img/pixel.gif" class="pix"></TD>\
              <TD class="mid" bgcolor="#e5e5e5" width="'+midSize+'px"><img src="/img/pixel.gif" class="pix"></TD>\
              <TD class="trans" bgcolor="#d7d7d7"><img src="/img/pixel.gif" class="pix"></TD>\
              <TD class="end" bgcolor="#cecece"><img src="/img/pixel.gif" class="pix"></TD>\
            </TR>\
          </TABLE>';
};

