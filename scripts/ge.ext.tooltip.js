(function( $ ){
  $.fn.geTooltip = function() {

    var $tooltip;

    this.parent().bind('mouseenter', function() {
        $tooltip = $('.ge-chrext-tooltip', $(this)).clone().appendTo("#ge-chrext");

        $tooltip.css('width', "");

        var tooltipWidth = $tooltip.width() + 1;
        var tooltipTop = Math.round($(this).offset().top - ($tooltip.height() + 10));

        if ($tooltip.hasClass("center")) {
          var tooltipSide = Math.round(($(this).offset().left + $(this).width()/2) - tooltipWidth/2);
        } else if ($tooltip.hasClass("left")) {
          var tooltipSide = Math.round($(this).offset().left - 5);
        } else if ($tooltip.hasClass("right")) {
          var tooltipSide = Math.round($(this).offset().left + $(this).width() - tooltipWidth + 5);
        }

        $tooltip.css({
          'display': 'block',
          'left': tooltipSide,
          'top': tooltipTop,
          'width': tooltipWidth
        });
    }).bind('mouseleave', function() {
        if ($tooltip)
          $tooltip.remove();
      });
  };
})( jQuery );

